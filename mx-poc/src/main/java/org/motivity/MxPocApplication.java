package org.motivity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MxPocApplication {

    public static void main(String[] args) {
        SpringApplication.run(MxPocApplication.class, args);
    }

}