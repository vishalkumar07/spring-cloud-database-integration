package org.motivity.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class HttpConfiguration {

    @Value("${mx.auth.value}")
    private String auth;

    @Bean("mxWebClient")
    public WebClient getMxWebClient() {
        return WebClient
                .builder()
                .baseUrl("https://int-api.mx.com")
                .defaultHeaders(httpHeaders -> {
                    httpHeaders.add(HttpHeaders.AUTHORIZATION, "Basic " + auth);
                    httpHeaders.add(HttpHeaders.ACCEPT, MediaType.valueOf("application/vnd.mx.api.v1+json").toString());
                    httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
                })
                // Codecs have limits for buffering data in memory to avoid application memory issues. By default, those are set to 256KB. we are setting that size to unlimited.
                .exchangeStrategies(
                        ExchangeStrategies.builder()
                                .codecs(clientCodeConfigure ->
                                        clientCodeConfigure
                                                .defaultCodecs()
                                                .maxInMemorySize(-1) // unlimited memory size
                                )
                                .build()
                ).build();
    }
}