package org.motivity.service;

import org.motivity.model.Provider;
import org.motivity.model.User;
import org.motivity.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

@Service
public class CustomOAuth2UserService extends DefaultOAuth2UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Override
    public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {

        OAuth2User oAuth2User = super.loadUser(userRequest);
        String email = (String) oAuth2User.getAttributes().get("email");

        if(email.isEmpty()) {
            throw new OAuth2AuthenticationException("Email not found from OAuth2 provider Facebook");
        }

        User existingUser = userRepository.findUserByEmail(email);

        if (existingUser == null) {
            System.out.println("creating user");
            registerNewUser(email);
        }

        return oAuth2User;
    }


    private User registerNewUser(String email) {
        User user = userService.createUserInMx(email);
        user.setProvider(Provider.FACEBOOK);
        return userRepository.save(user);
    }
}
