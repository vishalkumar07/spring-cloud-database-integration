package org.motivity.service;

import org.motivity.model.CreateUser;
import org.motivity.model.MxUserResponse;
import org.motivity.model.User;
import org.motivity.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.ArrayList;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    @Qualifier("mxWebClient")
    private WebClient mxWebClient;

    @Autowired
    private UserRepository userRepository;

    public User getUser() {
        String userGuid = "USR-9b94832d-5197-4cb0-b8ec-9ec2e2636eb6";
        String result = mxWebClient
                .get()
                .uri("/users/" + userGuid)
                .retrieve()
                .bodyToMono(String.class)
                .block();
        System.out.println(result);
        return new User();
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findUserByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), new ArrayList<>());
    }

    public User createUserInMx(String email) {
        CreateUser createUser = new CreateUser();
        createUser.setMxUser(createUser.new MxUser(email));

        MxUserResponse user = mxWebClient
                .post()
                .uri("/users")
                .body(Mono.just(createUser), CreateUser.class)
                .retrieve()
                .bodyToMono(MxUserResponse.class)
                .block();
        System.out.println(user.getUser());
        return user.getUser();
    }
}
