package org.motivity.controller;

import org.motivity.model.User;
import org.motivity.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET, value = "/test")
    public User insertUser() {
        return userService.getUser();
    }


    @RequestMapping(method = RequestMethod.GET, value = "/testOne")
    public String insertUserOne() {
        return "testingOne";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/testTwo")
    public String insertUserTwo() {
        return "testingTwo";
    }

}
