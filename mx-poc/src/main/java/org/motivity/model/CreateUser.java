package org.motivity.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateUser {

    @JsonProperty(value = "user")
    private MxUser mxUser;

    public CreateUser() {
    }

    public CreateUser(MxUser mxUser) {
        this.mxUser = mxUser;
    }

    public MxUser getMxUser() {
        return mxUser;
    }

    public void setMxUser(MxUser mxUser) {
        this.mxUser = mxUser;
    }


    public class MxUser {

        private String email;

        public MxUser() {
        }

        public MxUser(String email) {
            this.email = email;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }
}

