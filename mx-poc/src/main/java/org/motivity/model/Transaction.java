package org.motivity.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "transactionTable")
public class Transaction {

    @Id
    private String transactionGuid;
    private String userGuid;
    private String accountGuid;
    private String memberGuid;
    private String accountId;
    private String transactionId;
    private String category;
    private String merchantGuid;
    private String type;
    private LocalDateTime updatedAt;
    private String currencyCode;
    private LocalDateTime transactedAt;
    private LocalDate date;
    private boolean isSubscription;
    private double amount;
    private String status;
    private String description;

    public Transaction() {}

    public Transaction(String transactionGuid, String userGuid, String accountGuid, String memberGuid, String accountId, String transactionId, String category, String merchantGuid, String type, LocalDateTime updatedAt, String currencyCode, LocalDateTime transactedAt, LocalDate date, boolean isSubscription, double amount, String status, String description) {
        this.transactionGuid = transactionGuid;
        this.userGuid = userGuid;
        this.accountGuid = accountGuid;
        this.memberGuid = memberGuid;
        this.accountId = accountId;
        this.transactionId = transactionId;
        this.category = category;
        this.merchantGuid = merchantGuid;
        this.type = type;
        this.updatedAt = updatedAt;
        this.currencyCode = currencyCode;
        this.transactedAt = transactedAt;
        this.date = date;
        this.isSubscription = isSubscription;
        this.amount = amount;
        this.status = status;
        this.description = description;
    }

    public String getTransactionGuid() {
        return transactionGuid;
    }

    public void setTransactionGuid(String transactionGuid) {
        this.transactionGuid = transactionGuid;
    }

    public String getUserGuid() {
        return userGuid;
    }

    public void setUserGuid(String userGuid) {
        this.userGuid = userGuid;
    }

    public String getAccountGuid() {
        return accountGuid;
    }

    public void setAccountGuid(String accountGuid) {
        this.accountGuid = accountGuid;
    }

    public String getMemberGuid() {
        return memberGuid;
    }

    public void setMemberGuid(String memberGuid) {
        this.memberGuid = memberGuid;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMerchantGuid() {
        return merchantGuid;
    }

    public void setMerchantGuid(String merchantGuid) {
        this.merchantGuid = merchantGuid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public LocalDateTime getTransactedAt() {
        return transactedAt;
    }

    public void setTransactedAt(LocalDateTime transactedAt) {
        this.transactedAt = transactedAt;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public boolean isSubscription() {
        return isSubscription;
    }

    public void setSubscription(boolean subscription) {
        isSubscription = subscription;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
