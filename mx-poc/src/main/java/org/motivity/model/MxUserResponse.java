package org.motivity.model;

public class MxUserResponse {
    private User user;

    public MxUserResponse() {}

    public MxUserResponse(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "MxUserResponse{" +
                "user=" + user +
                '}';
    }
}
