package org.motivity.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "userTable")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long userId;
    @JsonProperty("guid")
    private String userGuid;
    private String email;
    private String password;
    private LocalDateTime userCreatedAt;
    @Enumerated(EnumType.STRING)
    private Provider provider;

    @JoinColumn
    @OneToMany(cascade = CascadeType.ALL)
    private List<Member> members;

    public User() {
        this.userCreatedAt=LocalDateTime.now();
    }

    public User(long userId, String userGuid, String email, String password, LocalDateTime userCreatedAt, Provider provider, List<Member> members) {
        this.userId = userId;
        this.userGuid = userGuid;
        this.email = email;
        this.password = password;
        this.userCreatedAt = userCreatedAt;
        this.provider = provider;
        this.members = members;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserGuid() {
        return userGuid;
    }

    public void setUserGuid(String userGuid) {
        this.userGuid = userGuid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDateTime getUserCreatedAt() {
        return userCreatedAt;
    }

    public void setUserCreatedAt(LocalDateTime userCreatedAt) {
        this.userCreatedAt = userCreatedAt;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", userGuid='" + userGuid + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", userCreatedAt=" + userCreatedAt +
                ", provider=" + provider +
                ", members=" + members +
                '}';
    }
}