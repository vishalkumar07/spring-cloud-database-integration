package org.motivity.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "accountTable")
public class Account {

    @Id
    private String accountGuid;
    private String userGuid;
    private String memberGuid;
    private String accountId;
    private long accountNumber;
    private String accountType;
    private double balance;
    private LocalDateTime paymentDueAt;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private LocalDateTime lastPaymentAt;

    @JoinColumn
    @OneToMany(cascade = CascadeType.ALL)
    private List<Transaction> transactions;

    public Account() {}

    public Account(String accountGuid, String userGuid, String memberGuid, String accountId, long accountNumber, String accountType, double balance, LocalDateTime paymentDueAt, LocalDateTime createdAt, LocalDateTime updatedAt, LocalDateTime lastPaymentAt, List<Transaction> transactions) {
        this.accountGuid = accountGuid;
        this.userGuid = userGuid;
        this.memberGuid = memberGuid;
        this.accountId = accountId;
        this.accountNumber = accountNumber;
        this.accountType = accountType;
        this.balance = balance;
        this.paymentDueAt = paymentDueAt;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.lastPaymentAt = lastPaymentAt;
        this.transactions = transactions;
    }

    public String getAccountGuid() {
        return accountGuid;
    }

    public void setAccountGuid(String accountGuid) {
        this.accountGuid = accountGuid;
    }

    public String getUserGuid() {
        return userGuid;
    }

    public void setUserGuid(String userGuid) {
        this.userGuid = userGuid;
    }

    public String getMemberGuid() {
        return memberGuid;
    }

    public void setMemberGuid(String memberGuid) {
        this.memberGuid = memberGuid;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public LocalDateTime getPaymentDueAt() {
        return paymentDueAt;
    }

    public void setPaymentDueAt(LocalDateTime paymentDueAt) {
        this.paymentDueAt = paymentDueAt;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public LocalDateTime getLastPaymentAt() {
        return lastPaymentAt;
    }

    public void setLastPaymentAt(LocalDateTime lastPaymentAt) {
        this.lastPaymentAt = lastPaymentAt;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }
}
