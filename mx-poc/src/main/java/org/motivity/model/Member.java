package org.motivity.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "memberTable")
public class Member {

    @Id
    private String memberGuid;
    private String userGuid;
    private String email;
    private String institutionCode;
    private String institutionName;
    private String connectionStatus;
    private LocalDateTime aggregatedAt;
    private boolean isBeingAggregated;
    private LocalDateTime successfulAggregatedAt;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    @JoinColumn
    @OneToMany(cascade = CascadeType.ALL)
    private List<Account> accounts;

    public Member() {}

    public Member(String memberGuid, String userGuid, String email, String institutionCode, String institutionName, String connectionStatus, LocalDateTime aggregatedAt, boolean isBeingAggregated, LocalDateTime successfulAggregatedAt, LocalDateTime createdAt, LocalDateTime updatedAt, List<Account> accounts) {
        this.memberGuid = memberGuid;
        this.userGuid = userGuid;
        this.email = email;
        this.institutionCode = institutionCode;
        this.institutionName = institutionName;
        this.connectionStatus = connectionStatus;
        this.aggregatedAt = aggregatedAt;
        this.isBeingAggregated = isBeingAggregated;
        this.successfulAggregatedAt = successfulAggregatedAt;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.accounts = accounts;
    }

    public String getMemberGuid() {
        return memberGuid;
    }

    public void setMemberGuid(String memberGuid) {
        this.memberGuid = memberGuid;
    }

    public String getUserGuid() {
        return userGuid;
    }

    public void setUserGuid(String userGuid) {
        this.userGuid = userGuid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInstitutionCode() {
        return institutionCode;
    }

    public void setInstitutionCode(String institutionCode) {
        this.institutionCode = institutionCode;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getConnectionStatus() {
        return connectionStatus;
    }

    public void setConnectionStatus(String connectionStatus) {
        this.connectionStatus = connectionStatus;
    }

    public LocalDateTime getAggregatedAt() {
        return aggregatedAt;
    }

    public void setAggregatedAt(LocalDateTime aggregatedAt) {
        this.aggregatedAt = aggregatedAt;
    }

    public boolean isBeingAggregated() {
        return isBeingAggregated;
    }

    public void setBeingAggregated(boolean beingAggregated) {
        isBeingAggregated = beingAggregated;
    }

    public LocalDateTime getSuccessfulAggregatedAt() {
        return successfulAggregatedAt;
    }

    public void setSuccessfulAggregatedAt(LocalDateTime successfulAggregatedAt) {
        this.successfulAggregatedAt = successfulAggregatedAt;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }
}
