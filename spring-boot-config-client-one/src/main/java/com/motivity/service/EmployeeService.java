package com.motivity.service;

import com.motivity.model.Employee;
import com.motivity.repository.EmployeeRepository;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @PersistenceContext
    private EntityManager jpaEntityMgr;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private Session session;

    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    public void saveEmployee(Employee employee) {
        List<Employee> employee1 = employeeRepository.findByEmployeeName(employee.getEmployeeName());
        if(employee1.isEmpty()) {
            employeeRepository.save(employee);
        }
    }

    public Optional<Employee> getEmployeeById(String employeeId) {
        return employeeRepository.findById(employeeId);
    }

    public void deleteEmployee(String employeeId) {
        employeeRepository.deleteById(employeeId);
    }

    public List<Employee> getAllEmployeesHQL() {
        String query = "from Employee e";
        return jpaEntityMgr.createQuery(query).getResultList();
    }

    public List<Employee> getAllEmployeesJdbc() {
        String query = "select * from Employee";
        return jdbcTemplate.query(
                query,
                (rs, rowNum) ->
                        new Employee(
                                rs.getString("employee_id"),
                                rs.getString("employee_name")
                        )
        );
    }

    public List<Employee> getAllEmployeesSpringDataJPA() {
        return employeeRepository.findAllEmployeesSpringDataJPA();
    }

    public List<Employee> getAllEmployeesHibernate() {
        List<Employee> employeeList = session.createQuery("from Employee", Employee.class).list();
        return employeeList;
    }
}
