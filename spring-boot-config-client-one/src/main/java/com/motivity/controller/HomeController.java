package com.motivity.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @Value("${app.message: msg not found in properties file}")
    private String message;

    @Value("${app.message.one: msg not found in spring-boot-config-client-one properties file}")
    private String messageOne;

    @GetMapping("/home-one")
    public String home() {
        return message + "<br/>" + messageOne;
    }
}
