package com.motivity.controller;

import com.motivity.model.Employee;
import com.motivity.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @GetMapping("/all")
    public List<Employee> getAllEmployees() {
        return employeeService.getAllEmployees();
    }

    @PostMapping("/save")
    public void saveEmployee(@RequestBody Employee employee) {
        if (!employee.getEmployeeName().isEmpty())
            employeeService.saveEmployee(employee);
    }

    @GetMapping("/{employeeId}")
    public Optional<Employee> getEmployeeById(@PathVariable String employeeId) {
        return employeeService.getEmployeeById(employeeId);
    }

    @DeleteMapping("/delete/{employeeId}")
    public void deleteEmployee(@PathVariable String employeeId) {
        employeeService.deleteEmployee(employeeId);
    }

    @GetMapping("/hql")
    public List<Employee> getAllEmployeesHQL() {
        return employeeService.getAllEmployeesHQL();
    }

    @GetMapping("/jdbc")
    public List<Employee> getAllEmployeesJdbc() {
        return employeeService.getAllEmployeesJdbc();
    }

    @GetMapping("/spring-data-jpa")
    public List<Employee> getAllEmployeesSpringDataJPA() {
        return employeeService.getAllEmployeesSpringDataJPA();
    }

    @GetMapping("/hibernate")
    public List<Employee> getAllEmployeesHibernate() {
        return employeeService.getAllEmployeesHibernate();
    }
}
