package com.motivity.repository;

import com.motivity.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, String> {
    List<Employee> findByEmployeeName(String employeeName);

    @Query("from Employee e")
    List<Employee> findAllEmployeesSpringDataJPA();
}
