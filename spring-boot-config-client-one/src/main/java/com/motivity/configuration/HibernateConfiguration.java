package com.motivity.configuration;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.persistence.EntityManager;

@Configuration
public class HibernateConfiguration {

    @Autowired
    private EntityManager entityManager;

    @Bean
    public Session session() {
        return entityManager.unwrap(Session.class);
    }
}
